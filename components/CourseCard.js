import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({ courseProp }) {
    const {name, description, price, start_date, end_date} = courseProp;

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(10);
    const [isOpen, setIsOpen] = useState(true);

    function enroll(){
        setCount(count + 1);
        console.log('Enrollees: ' + count);
        setSeats(seats - 1);
        console.log('Seats: ' + seats);
    }

    useEffect(() => {
        if(seats === 0){
            setIsOpen(false);
        }
    }, [seats]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>
                    <span className="subtitle">Description:</span>
                    <br />
                    {description}
                    <br />
                    <span className="subtitle">Price: </span>
                    PHP {price}
                </Card.Text>

                {isOpen ? 
                    <Button className="bg-primary" onClick={enroll}>Enroll</Button>
                :
                    <Button className="bg-danger" disabled>Not Available</Button>
                }
                
            </Card.Body>
        </Card>
    )
}

CourseCard.propTypes = {
    // shape() - used to check that the prop conforms 
    // to a specific "shape"
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}